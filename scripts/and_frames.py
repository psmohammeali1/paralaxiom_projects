from bdb import effective
import cv2
import os
import time
def main():
    file = open("and_frame_report.txt", "w+")
    total_time = 0
    total_frames = 0
    input_path = "output/"
    folder_list = os.listdir(input_path)
    for folder in folder_list:
        # print(folder)
        aimodel_folder = input_path+folder+"/aimodel_0.4/"
        bg_folder = input_path+folder+"/filter/"
        
        aimodel_images = os.listdir(aimodel_folder)
        bg_images = os.listdir(bg_folder)
        
        length = len(aimodel_images)
        for i in range(1,length+1):
            aimodel_name = str(i)+" aimodel.png"
            bg_name = str(i)+ " filter.png"
            img1 = cv2.imread(aimodel_folder+aimodel_name)
            img2 = cv2.imread(bg_folder+bg_name)
            print(aimodel_folder+aimodel_name)
            print(bg_folder+bg_name)

            start = time.time()
            bitwiseAnd = cv2.bitwise_and(img1, img2)
            end = time.time()
            effective_time = end - start
            total_time = total_time + effective_time
            total_frames = total_frames + 1
            print(i)
            file.write("Video Name : {} \n".format(folder))
            file.write("Current Video Frame: {} \n".format(i))
            file.write("Frame Effective Time: {} \n".format(effective_time))
            file.write("-------\n")

            print("Total Frames: ", total_frames)
            print("Total Time: ",total_time)
            
            # cv2.imshow("jdlkf",bitwiseAnd)
            # cv2.waitKey(0)
            cv2.imwrite(input_path+folder+"/and/"+str(i)+" and.png", bitwiseAnd)
            # print(input_path+folder+"/out/"+str(i)+" and.png")
            # print("-----")
            print(i)
    return[total_frames,total_time]

ans = main()  
my_file = open("and_report.txt", "w+")
my_file.write("Total Frames: {}".format(ans[0]))
my_file.write("Total Time Taken : {}".format(ans[1]))
my_file.close()

